const isProdMode = Object.is(process.env.NODE_ENV, 'production')

export default {
  baseUrl: isProdMode ? 'http://api.yenbase.com/' : 'http://192.168.1.102:8000/'
}
