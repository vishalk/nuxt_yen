const isProdMode = Object.is(process.env.NODE_ENV, 'production')

module.exports = {
  /*
  ** Build configuration
  */
  build: {
    // vendor: ['axios'],
    postcss: {
      plugins: {
        'postcss-custom-properties': {
          warnings: false
        }
      }
    }
  },
  /*
  ** Headers
  ** Common headers are already provided by @nuxtjs/pwa preset
  */
  head: {
    title: 'Yenbase',
    link: [
      { rel: 'shortcut icon', type: 'image/x-icon', sizes: '48x48', href: '/logo-48.png' },
      {
        rel: 'stylesheet',
        href: '//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css'
        // href: '//fonts.googleapis.com/icon?family=Material+Icons'
      }
    ]
  },
  /*
  ** Custom css
  */
  css: [
    '@/assets/css/main.scss'
  ],
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#ffffff' },
  /*
  ** Customize app manifest
  */
  manifest: {
    theme_color: '#7957d5'
  },
  /*
  ** Modules
  */
  modules: [
    '@nuxtjs/pwa',
    '@nuxtjs/axios'
  ],
  /*
  ** Plugins
  */
  plugins: [
    '~/plugins/buefy',
    '~/plugins/vuex-router-sync',
    '~/plugins/vue-timeago'
  ],
  /*
  ** Router config
  */
  router: {
    middleware: [
      'check-auth',
      'change-page-col'
    ]
  },
  /*
  ** Environment variables
  */
  env: {
    facebookClientId: '285795155262899',
    googleClientId: ''
  },
  /*
  ** Axios config
  */
  axios: {
    // baseURL: isProdMode ? 'http://api.yenbase.com/' : 'http://192.168.1.102:8000/',
    baseURL: isProdMode ? 'http://api.yenbase.com/' : 'http://localhost:8000/',
    requestInterceptor: (config, { store }) => {
      if (store.state.auth && store.state.auth.token) {
        // console.log(store.state.auth.token)
        config.headers.common['Authorization'] = 'Token ' + store.state.auth.token
      }
      return config
    }
  }
}
