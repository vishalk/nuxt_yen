// import jwtDecode from 'jwt-decode'
import Cookie from 'js-cookie'

const getQueryParams = (url) => {
  const params = {}
  url.replace(/([^(?|#)=&]+)(=([^&]*))?/g, ($0, $1, $2, $3) => {
    params[$1] = $3
  })
  return params
}

export const extractInfoFromHash = (url) => {
  if (process.BROWSER_BUILD) return
  const { access_token } = getQueryParams(window.location.href)
  return {
    access_token: access_token
  }
}

export const setToken = (token) => {
  if (process.SERVER_BUILD) return
  window.localStorage.setItem('token', token)
  // window.localStorage.setItem('user', JSON.stringify(jwtDecode(token)))
  Cookie.set('token', token)
}

export const unsetToken = () => {
  if (process.SERVER_BUILD) return
  window.localStorage.removeItem('token')
  // window.localStorage.removeItem('user')
  Cookie.remove('token')
  window.localStorage.setItem('logout', Date.now())
}

export const getTokenFromCookie = (req) => {
  if (!req.headers.cookie) return
  const tokenCookie = req.headers.cookie.split(';').find(c => c.trim().startsWith('token='))
  if (!tokenCookie) return
  const token = tokenCookie.split('=')[1]
  return token
}

export const getTokenFromLocalStorage = (req) => {
  const token = window.localStorage.token
  return token
}
