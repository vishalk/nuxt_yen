export default {
  tokenName: 'token',
  tokenPrefix: 'yenauth',
  tokenHeader: 'Authorization',
  tokenType: 'Token',

  /**
   * Default request interceptor for Axios library
   */
  bindRequestInterceptor: function ($auth) {
    const tokenHeader = $auth.options.tokenHeader

    $auth.$http.interceptors.request.use((config) => {
      if ($auth.isAuthenticated()) {
        config.headers[tokenHeader] = [
          $auth.options.tokenType, $auth.getToken()
        ].join(' ')
      } else {
        delete config.headers[tokenHeader]
      }
      return config
    })
  },

  /**
   * Default response interceptor for Axios library
   */
  bindResponseInterceptor: function ($auth) {
    $auth.$http.interceptors.response.use((response) => {
      $auth.setToken(response)
      return response
    })
  },

  providers: {
    'facebook': {
      name: 'facebook',
      redirectUrl: 'auth/facebook/callback',
      authorizationEndpoint: 'https://www.facebook.com/v2.11/dialog/oauth',
      clientId: process.env.facebookClientId,
      scope: ['email', 'public_profile'],
      scopeDelimiter: ',',
      requiredUrlParams: ['scope']
    },

    'google-oauth2': {
      name: 'google',
      redirectUrl: 'auth/google-oauth2/callback',
      authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
      clientId: process.env.googleClientId,
      requiredUrlParams: ['scope'],
      scope: ['profile', 'email'],
      scopePrefix: 'openid',
      scopeDelimiter: ' '
    }
  }
}
