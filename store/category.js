
export const state = () => {
  return {
    list: {
      fetching: false,
      data: {
        data: []
      }
    }
  }
}

export const getters = {
  songCategories (state) {
    const categories = state.list.data
    return categories[2]
  },
  movieCategories (state) {
    const categories = state.list.data
    return categories[1]
  }
}

export const mutations = {
  REQUEST_LIST (state) {
    state.fetching = true
  },
  GET_LIST_FAILURE (state) {
    state.fetching = false
  },
  GET_LIST_SUCCESS (state, action) {
    state.fetching = false
    state.list.data = action
  }
}
