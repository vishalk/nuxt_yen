
export const state = () => {
  return {
    // authUser: null,
    token: null
  }
}

export const getters = {
  isAuthenticated (state) {
    return !!state.token
  }
}

export const mutations = {
  SET_AUTHENTICATION (state, token) {
    state.token = token
    // state.authUser = user
  },

  CLEAR_AUTHENTICATION (state) {
    // state.authUser = null
    state.token = null
  }
}
