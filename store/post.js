
export const state = () => {
  return {
    trending: {
      fetching: false,
      data: {
        data: []
      }
    },
    list: {
      fetching: false,
      data: {
        pagination: {
          current_page: 0
        },
        data: []
      }
    },
    detail: {
      fetching: false,
      data: {}
    },
    create: {
      fetching: false,
      searching: false,
      posting: false,
      songPost: {
        searchResults: []
        // selectedSong: null
      },
      moviePost: {},
      linkPost: {},
      textPost: {}
    }
  }
}

export const mutations = {

  // All posts
  CLEAR_LIST (state) {
    state.list.data = {
      result: {
        pagination: {
          current_page: 0
        },
        data: []
      }
    }
  },

  REQUEST_LIST (state) {
    state.list.fetching = true
  },

  GET_LIST_FAILURE (state) {
    state.list.fetching = false
  },

  GET_LIST_SUCCESS (state, action) {
    state.list.fetching = false
    state.list.data = action.results
  },

  ADD_LIST_SUCCESS (state, action) {
    state.list.fetching = false
    state.list.data.data.push.apply(state.list.data.data, action.results)
    state.list.data.pagination = action.results.pagination
  },

  // Song search
  REQUEST_SONG_SEARCH (state) {
    state.create.searching = true
  },

  GET_SONG_SEARCH_FAILURE (state) {
    state.create.searching = false
  },

  GET_SONG_SEARCH_SUCCESS (state, action) {
    state.create.searching = false
    console.log(action.results)
    state.create.songPost.searchResults = action.results
  },

  CLEAR_SONG_SEARCH (state) {
    state.create.songPost.searchResults = []
  },

  // Create post
  POST_SONGPOST (state) {
    state.create.posting = true
  },

  POST_SONGPOST_FAILURE (state) {
    state.create.posting = false
  },

  POST_SONGPOST_SUCCESS (state, action) {
    state.create.posting = false
  },

  // Get post
  REQUEST_DETAIL (state) {
    state.detail.fetching = true
  },

  GET_DETAIL_FAILURE (state) {
    state.detail.fetching = false
    state.detail.data = {}
  },

  GET_DETAIL_SUCCESS (state, data) {
    state.detail.fetching = false
    state.detail.data = data
  }
}
