
export const state = () => {
  return {
    fullColumn: false
  }
}

export const mutations = {
  SET_FULL_COLUMN (state, action) {
    state.fullColumn = action
  }
}
