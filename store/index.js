// import Service from '~/plugins/axios'
// import Service from '../plugins/axios'
import options from '../utils/auth/options'

export const actions = {
  nuxtServerInit (store, { commit, params, route, isServer, req }) {
    // if (req.session && req.session.authUser) {
    //   commit('SET_USER', req.session.authUser)
    // }
  },

  authenticate ({ commit, redirect }, provider) {
    const providerConfig = options.providers[provider]

    const endpoint = providerConfig.authorizationEndpoint
    const clientId = providerConfig.clientId
    const redirectUri = window.location.origin + '/' + providerConfig.redirectUrl
    const scope = providerConfig.scope.join(',')

    const url = `${endpoint}?client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scope}&response_type=token`

    window.location.href = url
  },

  exchange_token ({ commit }, params) {
    const uri = `auth/${params.provider}/`

    return this.$axios.post(uri, { access_token: params.access_token })
      .then(response => {
        const success = !!response.status && response.data && response.data.token
        if (success) {
          commit('auth/SET_AUTHENTICATION', response.data.token)
          return Promise.resolve(response.data)
        } else {
          commit('auth/CLEAR_AUTHENTICATION')
          return Promise.reject(response.data)
        }
      }, err => {
        commit('auth/CLEAR_AUTHENTICATION')
        return Promise.reject(err)
      })
  },

  loadCategories ({ commit }) {
    commit('category/REQUEST_LIST')

    return this.$axios.get('categories/get_categories/')
      .then(response => {
        const success = !!response.status && !!response.data
        if (success) commit('category/GET_LIST_SUCCESS', response.data)
        if (!success) commit('category/GET_LIST_FAILURE')
      }, err => {
        commit('category/GET_LIST_FAILURE', err)
      })
  },

  loadPosts ({ commit }, params = { page: 1 }) {
    commit('post/REQUEST_LIST')

    return this.$axios.get('posts/', { params })
      .then(response => {
        const success = !!response.status && !!response.data
        const isFirstPage = params.page && params.page > 1
        const commitName = `post/${isFirstPage ? 'ADD' : 'GET'}_LIST_SUCCESS`
        if (success) commit(commitName, response.data)
        if (!success) commit('post/GET_LIST_FAILURE')
      }, err => {
        commit('post/GET_LIST_FAILURE', err)
      })
  },

  fetchPost ({ commit, state }, { id }) {
    commit('post/REQUEST_DETAIL')

    return this.$axios.get(`posts/${id}`)
      .then(response => {
        const success = !!response.status && !!response.data
        if (success) commit('post/GET_DETAIL_SUCCESS', response.data)
        if (!success) commit('post/GET_DETAIL_FAILURE')
        return Promise.resolve(response.data)
      }, err => {
        commit('post/GET_DETAIL_FAILURE', err)
        return Promise.reject(err)
      })
  },

  searchSong ({ commit }, params) {
    commit('post/REQUEST_SONG_SEARCH')

    return this.$axios.post('media/search-song/', params)
      .then(response => {
        // const success = response.status === 200 && response.data && response.data.results
        const success = response.status === 200 && response.data
        if (success) commit('post/GET_SONG_SEARCH_SUCCESS', response.data)
        if (!success) commit('post/GET_SONG_SEARCH_FAILURE')
      }, err => {
        commit('post/GET_SONG_SEARCH_FAILURE', err)
      })
  },

  postSongPost ({ commit }, songPost) {
    commit('post/POST_SONGPOST')

    return this.$axios.post('posts/', songPost)
      .then(response => {
        const success = !!response.status && response.data && Object.is(response.status, 201)
        if (success) {
          commit('post/POST_SONGPOST_SUCCESS', response.data)
          return Promise.resolve(response.data)
        } else {
          commit('post/POST_SONGPOST_FAILURE')
          return Promise.reject(response.data)
        }
      }, err => {
        commit('post/POST_SONGPOST_FAILURE', err)
        return Promise.reject(err)
      })
  }
}

