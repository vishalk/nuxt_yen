import { getTokenFromCookie, getTokenFromLocalStorage } from '~/utils/auth'

export default function ({ isServer, store, req }) {
  // if nuxt generate, skip the middleware
  if (isServer && !req) return
  const token = isServer ? getTokenFromCookie(req) : getTokenFromLocalStorage(req)
  store.commit('auth/SET_AUTHENTICATION', token)
}
