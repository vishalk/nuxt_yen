import Vue from 'vue'
import axios from 'axios'
import apiConfig from '../api.config'
// import options from '~/utils/auth/options'

const service = axios.create({
  baseURL: apiConfig.baseUrl
})

service.interceptors.request.use(config => {
  // const tokenHeader = options.tokenHeader

  // console.log(config)
  return config
}, error => {
  return Promise.reject(error)
})

service.interceptors.response.use(response => {
  return response
}, error => {
  return Promise.reject(error)
})

Vue.prototype.$http = axios

export default service
